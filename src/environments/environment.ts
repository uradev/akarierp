// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'akari-95946',
    appId: '1:300667138298:web:877d3c01756a6747725943',
    storageBucket: 'akari-95946.appspot.com',
    locationId: 'southamerica-east1',
    apiKey: 'AIzaSyArMzm70vtPbJ9lQowZR3g4CqzBs63EONM',
    authDomain: 'akari-95946.firebaseapp.com',
    messagingSenderId: '300667138298',
    measurementId: 'G-WJ953BWPB0',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
