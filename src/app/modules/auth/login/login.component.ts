import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, MinLengthValidator, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginForm = this.fb.group({
    email:['', Validators.required],
    password:['',[Validators.required, Validators.minLength(5)]], 
  });


  constructor(private fb:FormBuilder, private router:Router, private auth:AuthService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void { }

  login(){
  
    this.auth.loginUser(this.loginForm.value)
    .then(() => this.router.navigate(['/home']))
      .catch((e) => this.openSnackBar('Correo o contraseña invalido ','Ok'));
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000
    });
    
  }

}
