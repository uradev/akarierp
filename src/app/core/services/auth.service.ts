import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { LoginInterface } from '../interfaces/login-interface';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router, private angualrfire:AngularFireAuth) {

    }

    loginUser({email,password}:LoginInterface){
      return this.angualrfire.signInWithEmailAndPassword(email,password)
    }

    getUserLogin(){
      return this.angualrfire.authState;
    }       
    

    logout() {
      return this.angualrfire.signOut();
    }


}
