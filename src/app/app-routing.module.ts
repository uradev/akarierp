import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './modules/auth/login/login.component';
import { Page404Component } from './modules/page404/page404.component';


const routes: Routes = [
  //---- rutas en Lazy loading------ //
  { path:'',redirectTo:'/login',pathMatch: 'full'},
  { path: 'login',loadChildren: () => import('./modules/auth/login/login.module').then((m )=> m.LoginModule)},
  { path: 'home',loadChildren: () => import('./modules/home/home.module').then((m )=> m.HomeModule)},
  { path: 'admin/perfil',loadChildren: () => import('./modules/admin/perfil/perfil.module').then((m )=> m.PerfilModule)},
  {path:'**',component:Page404Component}


]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
