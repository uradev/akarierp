import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './componentes/header/header.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { MaterialUiModule } from './material-ui/material-ui.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    MaterialUiModule,
    RouterModule
  ],
  exports:[
    MaterialUiModule,
    HeaderComponent,
    FooterComponent
  ]
})
export class SharedModule { }
