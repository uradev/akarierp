import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
Router

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router, private auth:AuthService, private _snackBar: MatSnackBar) {

   }

  ngOnInit(): void {
  }



  logout() {
    this.auth.logout().then( res => {      
      this.openSnackBar('Gracias ','Ok')
      this.router.navigate(['/login'])
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000
    });
  }

}
