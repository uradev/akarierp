# Akarierp

Este proyecto esta generado con [Angular CLI](https://github.com/angular/angular-cli) version 13.3.6.

## Requerimientos
[NODE JS](https://nodejs.org/es/) 
[ANGULAR CLI](https://angular.io/cli) 

## Ejecurar proyecto
- Ejecutar `npm install` para instalar los paquetes requeridos
- Ejecutar `ng serve` para ejecutar el proyecto
- Ir la url [http://localhost:4200/](http://localhost:4200/) 



